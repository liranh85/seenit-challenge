# Installation: #
## From the terminal: ##
- npm i
- gulp

## In your browser: ##
- navigate to localhost:8080

# Notes: #

## Things I would have done if I spent more time on this task: ##
- Created the film details page
- Invoked the search function also when the search icon is clicked (not just when pressing Enter)
- Added a loader whilst fetching results, so the user knows something is coming