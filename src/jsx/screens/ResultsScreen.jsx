'use strict';

import React from 'react';
import SearchResult from '../components/SearchResult.jsx';

class ResultsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        console.log('ResultsScreen', this.props.filmData);
        let searchResults = [];
        
        this.props.filmData.forEach((value, index, array) => {
            searchResults.push(<SearchResult
                key = {index}
                filmTitle = {value.title}
                releaseYear = {value.releaseDate.getFullYear()}
                imgSrc = {value.posterUrl} />);
        });
        
        return (
            <div className="screen-container results-screen">
                <header>
                    <a href="/" title="Home">
                        <img src="images/logo.png" className="logo" alt="Logo"/>
                    </a>
                    <div className="search-input-container">
                        <input type="text" className="search" placeholder="Search" onKeyPress={(e) => this.props.onSearchKeyPress(e)} />
                        <span></span>
                    </div>
                </header>

                <ul className="results">
                    {searchResults}
                </ul>
            </div>
        );
    }

}

export default ResultsScreen;
