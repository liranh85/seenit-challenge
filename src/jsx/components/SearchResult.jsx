'use strict';

import React from 'react';
import config from '../../js/config.js';

class SearchResult extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imgSrc: this.props.imgSrc
        };
    }

    componentWillReceiveProps() {
        this.setState({imgSrc: this.props.imgSrc});
    }

    render() {
        let image = new Image();

        image.onerror = () => {
            this.setState({imgSrc: config.noPosterPath});
        }
        image.src = this.state.imgSrc;

        return (
            <li>
                <div className="img-container">
                    <img src={this.state.imgSrc} alt="poster"/>
                </div>
                <div className="text-container">
                    <h1 className="film-title">{this.props.filmTitle}</h1>
                    <h2 className="film-year">{this.props.releaseYear}</h2>
                </div>
            </li>
        );
    }

}

export default SearchResult;
