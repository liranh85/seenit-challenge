'use strict';

import React from 'react';

class CompleteScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="screen-container search-screen">
                <div className="logo-and-search">
                    <img src="images/logo.png" className="logo" alt="Logo"/>
                    <div className="search-input-container">
                        <input type="text" className="search" placeholder="Search" onKeyPress={(e) => this.props.onSearchKeyPress(e)} />
                        <span></span>
                    </div>
                </div>
            </div>
        );
    }

}

export default CompleteScreen;
