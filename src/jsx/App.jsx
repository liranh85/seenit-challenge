'use strict';

import React from 'react';
import appStates from './constants/appStates.jsx';
import SearchScreen from './screens/SearchScreen.jsx';
import ResultsScreen from './screens/ResultsScreen.jsx';
import config from '../js/config.js';

class App extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			currentScreen: appStates.SEARCH_SCREEN,
			results: []
		};

		this.data = {
			
		};

		this.submitQuery = this.submitQuery.bind(this);
		this.processQuery = this.processQuery.bind(this);
		this.searchKeyPressHandler = this.searchKeyPressHandler.bind(this);
	}

	submitQuery(query) {
		return new Promise((resolve, reject) => {
			theMovieDb.search.getMovie({"query": query}, data => {
				resolve({data});
			}, error => {
				reject({error: error});
			});
		});
	}
	processQuery(response) {
		return new Promise((resolve, reject) => {
			const data = JSON.parse(response.data);
			console.log(data);
			const results = data.results;
			let resultsForState = [];
			
			results.forEach((value, index, array) => {
				let currentResult = {}
				currentResult.posterUrl = config.imageUrlBase + value.poster_path;
				currentResult.poster2Url = config.imageUrlBase + value.backdrop_path;
				currentResult.adult = value.adult;
				currentResult.overview = value.overview;
				currentResult.releaseDate = new Date(value.release_date);
				currentResult.title = value.title;
				currentResult.lang = value.original_language;
				currentResult.popularity = value.popularity;
				currentResult.voteCount = value.vote_count;

				resultsForState.push(currentResult);
			});
			// debugger;
			this.setState({results: resultsForState});
			resolve();
		});
	}

	searchKeyPressHandler(e) {
		if (!e) e = window.event;
		const keyCode = e.keyCode || e.which;
		if (keyCode == '13'){
			const query = e.target.value;
			this.submitQuery(query)
				.then(this.processQuery, error => console.log("Error", error))
				.then(() => this.setState({currentScreen: appStates.RESULTS_SCREEN}));
		}
	}

	render() {
		const appPartials = {
			[appStates.SEARCH_SCREEN]:
				<SearchScreen
					onSearchKeyPress = {this.searchKeyPressHandler} />,
			
			[appStates.RESULTS_SCREEN]:
				<ResultsScreen
					filmData = {this.state.results}
					onSearchKeyPress = {this.searchKeyPressHandler} />
		}
		
		let currentScreenComponent = appPartials[this.state.currentScreen];

		return (
			<div>
				{currentScreenComponent}
			</div>
		);
	}
}

export default App;