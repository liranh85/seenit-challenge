var config = {
    imageUrlBase: 'https://image.tmdb.org/t/p/w500',
    noPosterPath: '../../images/no-poster.png'
}

export default config;