const appStates = {
    SEARCH_SCREEN  : 'State.SEARCH_SCREEN',
    RESULTS_SCREEN : 'State.RESULTS_SCREEN'
};

export default appStates;